//This is a compilation of everyone's efforts
//Codes from TongFatt: Anything that is difficult is done by him e.g. date formatting, editing, sort by category etc.
//Codes from LimBT: The main codes were done by him e.g. deleting, editing, etc etc.
//Codes from Soo Cheng: Beautifying etc is done by her with some coding here and there.
//TongFatt didn't manage to commit his codes into this main file due to settings issues so you might not find records of his commit.
//We troubleshooted our bugs together as a team!!!


var path = require("path");
var express= require("express");

//load an instance of app express
var app = express();

//link to html file
app.use(express.static(path.join(__dirname,"public")));
app.use("/bower_components",express.static(path.join(__dirname,"bower_components")));

//set the port
app.set("port", parseInt(process.argv[2] || 3000));

//listen to port
app.listen(app.get("port"),function(){
    console.log("Application is on port %d at %s", app.get("port"), new Date())

})

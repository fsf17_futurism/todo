(function(){
    var TaskApp = angular.module("TaskApp", []);

    var TaskCtrl=function(){
        var TaskCtrl=this;
        TaskCtrl.task="";
        TaskCtrl.duedate=""; //input
        TaskCtrl.formattedDate=""; //output
        TaskCtrl.category="";
        TaskCtrl.newCategory="";
        TaskCtrl.status="";
        TaskCtrl.array=[];
        TaskCtrl.categoryLabel=["Uncategorized", "Full Stack Foundation", "Personal Development", "Errands" ];
        TaskCtrl.filter="";

// populate with sample data 
        TaskCtrl.array = [  {task:"Watch Youtube Video on Hair Styling",duedate:"18/02/2017", formattedDate:"18/02/2017", category:"Personal Development", status:"Pending" },
                            {task:"Complete group assignment",duedate:"01/03/2017",formattedDate:"01/03/2017", category:"Full Stack Foundation",status:"Pending"  },
                            {task:"Buy movie tickets",duedate:"01/04/2017", formattedDate:"01/04/2017",category:"Errands", status:"Pending" },
                            {task:"Set up an in-kitchen hydroponics farm",duedate:"01/04/2017", formattedDate:"01/04/2017",category:"Errands", status:"Pending" },
                            {task:"Buy tea for everyone",duedate:"01/04/2017", formattedDate:"01/04/2017",category:"Full Stack Foundation", status:"Pending" }
        ];
// adding a new task into the array of tasks
    TaskCtrl.add = function(){
            
            TaskCtrl.formattedDate = TaskCtrl.duedate.toLocaleDateString('en-GB'); //to change date format

            TaskCtrl.array.push( {
                task:TaskCtrl.task, 
                category:TaskCtrl.category, 
                duedate:TaskCtrl.duedate, 
                formattedDate:TaskCtrl.formattedDate,
                status: "Pending",  
            } );
            console.log(TaskCtrl.array);
    };    

// removing a task from the array of tasks
        TaskCtrl.delete = function(idx){
            console.log(idx);
            TaskCtrl.array.splice(idx,1);
        }


    // using findIndex of task matching for the 1st item in array will have problem when two or more tasks or same     
    // TaskCtrl.delete=function(placeholder){  
    //     var idx=TaskCtrl.array.findIndex(function(elem){
    //         return(placeholder == elem.task)
    //     })
    //     if (idx>=0){
    //         TaskCtrl.array.splice(idx,1);
    //     console.log(idx);
    //     }
         
    //         //splice(3,1,) means go to the 4th basket and remove 1, starting from it
    // }

//adding a new category
    TaskCtrl.addCategory=function(){
        TaskCtrl.categoryLabel.push(TaskCtrl.newCategory);
    console.log(TaskCtrl.categoryLabel);
}    

//adding a check function to mark completed Task
    TaskCtrl.check =function(idx){
        TaskCtrl.array[idx].status = "Done"

    }

// edit changes


TaskCtrl.newField = [];
    TaskCtrl.editing = false;
 TaskCtrl.editArray = function(idx) {
        console.log(idx);  
        
        console.log(TaskCtrl.array[idx]);
        TaskCtrl.newField[idx] = angular.copy(TaskCtrl.array[idx]);
    }


// cancel changes in editing

   
    TaskCtrl.cancel = function(index) {

           TaskCtrl.array[index] = TaskCtrl.newField[index];
            TaskCtrl.editing = false;
        
    };
 


///the } below belongs to the controller

    }

    TaskApp.controller("TaskCtrl", [TaskCtrl])

})();